package rpc;

/**
 * RPC协议接口, Client 与 Server端共同遵守
 * 
 * @author 宋挺
 */
public interface Login {
	/**
	 * 抽象方法 login(), 模拟用户登录传入两个String 类型的参数, 返回 String类型的结果
	 * 
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return 返回登录结果
	 */
	public String login(String username, String password);
}
