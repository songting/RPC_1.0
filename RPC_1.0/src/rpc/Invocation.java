package rpc;

import java.io.Serializable;

/**
 * RPC调用的封装, 包括以下字段: <br>
 * methodName: 方法名 <br>
 * parameterTypes: 方法参数列表的 Class 对象数组 <br>
 * params: 方法参数列表
 * 
 * @author 宋挺
 */
@SuppressWarnings("rawtypes")
public class Invocation implements Serializable {
	private static final long serialVersionUID = -7311316339835834851L;
	/** RPC协议接口的 Class对象 */
	private Class<?> intface;
	/** 方法名 */
	private String methodName;
	/** 方法参数列表的 Class 对象数组 */
	private Class[] parameterTypes;
	/** 方法的参数列表 */
	private Object[] params;

	public Invocation() {
	}

	/**
	 * 构造一个 RPC请求的封装
	 * 
	 * @param intface
	 *            RPC协议接口的 Class对象
	 * @param methodName
	 *            方法名
	 * @param parameterTypes
	 *            方法参数列表的 Class 对象数组
	 * @param params
	 *            方法的参数列表
	 */
	public Invocation(Class intface, String methodName, Class[] parameterTypes, Object[] params) {
		this.intface = intface;
		this.methodName = methodName;
		this.parameterTypes = parameterTypes;
		this.params = params;
	}

	public Class getIntface() {
		return intface;
	}

	public String getMethodName() {
		return methodName;
	}

	public Class[] getParameterTypes() {
		return parameterTypes;
	}

	public Object[] getParams() {
		return params;
	}
}
