package rpc;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 关闭资源工具
 * 
 * @author 宋挺
 */
public class CloseUtil {
	public static void closeAll(Closeable... all) {
		for (Closeable item : all) {
			if (null != item) {
				try {
					item.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void closeAll(Socket... all) {
		for (Socket item : all) {
			if (null != item) {
				try {
					item.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void closeAll(ServerSocket... all) {
		for (ServerSocket item : all) {
			if (null != item) {
				try {
					item.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
