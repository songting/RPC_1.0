package rpc;

/**
 * Server端 RPC协议接口( Login )的实现类
 * 
 * @author 宋挺
 */
public class LoginImpl implements Login {
	/**
	 * 实现 login()方法, 模拟用户登录
	 * 
	 * @param username
	 *            用户名
	 * @param password
	 *            密码
	 * @return hello 用户名
	 */
	@Override
	public String login(String username, String password) {
		return "hello " + username;
	}

}
